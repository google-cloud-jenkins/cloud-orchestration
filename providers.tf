terraform {
    required_providers {
        google = {
            source = "hashicorp/google"
            version = "4.14.0"
        }
    }
}

provider "google" {
    project = var.google-project
    region  = var.google-region
    zone    = var.google-zone
}

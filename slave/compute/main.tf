# This terraform module for creating three (03) to several 
# google VMs that will be used as slave nodes in a kubernetes cluster

#====================
# Declaring providers
#====================
terraform {
    required_providers {
        google = {
            source = "hashicorp/google"
            version = "4.14.0"
        }
    }
}

provider "google" {
    project = var.google-project
}

#=======================
# Declaring local values
#======================
locals {
    machine-type      = var.machine-type
}

#=============
# Slave nodes
#=============
resource "google_compute_instance" "slave-instance" {
    project        = var.google-project
    count          = var.number-slave
    name           = "slave-node-${count.index}"
    machine_type   = var.machine-type
    can_ip_forward = false
    metadata       = var.metadata
    zone           = var.google-zone
    boot_disk {
        initialize_params {
            image = var.machine-image
            size  = var.disk-size
            type  = var.disk-type
        }
        auto_delete  = var.auto-delete
        device_name  = var.device-name
        source       = var.disk-source
        mode         = var.disk-mode
    }
    dynamic "attached_disk" {
        for_each   = var.additional-disks
        content {
            device_name = attached_disk.value.device_name
            source      = attached_disk.value.source
            mode        = attached_disk.value.mode
        }
    }
    network_interface {
        network    = var.network 
        subnetwork = var.subnetwork
    }
    dynamic "network_interface" {
        for_each = var.additional-networks
        content {
            network    = network_interface.value.network
            subnetwork = network_interface.value.subnetwork
        }
    }
}

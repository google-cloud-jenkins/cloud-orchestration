#=============
# Creating networks
#=============
resource "google_compute_network" "public-network" {
    name                            = "public-network"
    project                         = var.google-project
    auto_create_subnetworks         = false
    delete_default_routes_on_create = false
    routing_mode                    = "REGIONAL"
    mtu                             = 1500
}

resource "google_compute_network" "private-network" {
    name                            = "private-network"
    project                         = var.google-project
    auto_create_subnetworks         = false
    delete_default_routes_on_create = false
    routing_mode                    = "REGIONAL"
    mtu                             = 1500
}

#===============
# Creating Subnets
#===============
resource "google_compute_subnetwork" "bastion-subnet" {
    name             = "bastion-subnet"
    ip_cidr_range    = var.bastion-subnet-ip-cidr
    network          = google_compute_network.public-network.id 
}

resource "google_compute_subnetwork" "master-subnet" {
    name             = "master-subnet"
    ip_cidr_range    = var.master-subnet-ip-cidr
    network          = google_compute_network.public-network.id 
}

resource "google_compute_subnetwork" "slave-subnet" {
    name             = "slave-subnet"
    ip_cidr_range    = var.slave-subnet-ip-cidr
    network          = google_compute_network.private-network.id 
}

#====================
# VPC Network Peering
#====================
resource "google_compute_network_peering" "public-to-private-peering" {
    name         = "public-to-private-peering"
    network      = google_compute_network.public-network.self_link
    peer_network =  google_compute_network.private-network.self_link
}

resource "google_compute_network_peering" "private-to-public-peering" {
    name         = "private-to-public-peering"
    network      = google_compute_network.private-network.self_link
    peer_network = google_compute_network.public-network.self_link
}

#===================
# Creating Firewalls
#===================
resource "google_compute_firewall" "public-firewall" {
    name    = "public-firewall"
    network = google_compute_network.public-network.name
    allow {
        protocol = "icmp"
    }
    allow {
        protocol = "tcp"
        ports    = [ "22" ]
    }
    source_ranges = ["0.0.0.0/0"]  
}

resource "google_compute_firewall" "private-denyall-firewall" {
    name    = "private-denyall-firewall"
    network =  google_compute_network.private-network.name
    deny {
        protocol = "all"
    }
    source_ranges = ["0.0.0.0/0","${var.bastion-subnet-ip-cidr}"]  
}

resource "google_compute_firewall" "private-allow-firewall" {
    name    = "private-allow-firewall"
    network =  google_compute_network.private-network.name
    allow {
        protocol = "icmp"
    }
    allow {
        protocol = "tcp"
        ports    = [ "22","443","80","8080" ]
    }
    source_ranges = ["${var.master-subnet-ip-cidr}"]  
}
